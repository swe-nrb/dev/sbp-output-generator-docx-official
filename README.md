# SBP Byggpaket med node

Detta byggpaket genererar en `.docx` och en `.html`-fil från en eller flera `.md`-filer i mappen `innehall`.

## Förvaltare

* [Johan Asplund](https://linkedin.com/in/bimjohan)

Saknas ditt namn här? Lämna isåfall en ändringsbegäran (merge request).

## Kom igång  

```
// i ett informationspaket

// ladda byggpaket
var byggpaket = require('sbp-byggpaket-node');

// kör
byggpaket();
```
