'use strict';

var _ = require('lodash');

module.exports = function() {
	console.log('Buildpack running...');

	var fs = require('fs');
	var pandoc = require('./pandoc');

	var contentDir = 'innehall';
	var outputDir = 'utdata';

	if (!fs.existsSync('./' + outputDir)){
	    fs.mkdirSync(outputDir);
	}

	// get file list
	var fileList = fs.readdirSync('./' + contentDir);

	var input = '';

	// loop over files
	_.forEach(fileList, function(file) {
		// only use .md files
		if(file.substr(file.length - 3) === '.md') {
			var content = fs.readFileSync('./' + contentDir + '/' + file, 'utf-8');
			input = input + content;
		}
	});

	// remove example boxes
	if(input.length > 0) {
		input = input.replace(/```[\s\S]+?```/gm, '')
	}

	// create docx
	pandoc(input, ['-f', 'markdown', '-t', 'docx', '-o', './' + outputDir + '/dokument.docx'], function(err) {
		if(err) {
			console.log(err);
		} else {
			console.log('Infopack built without errors!');
		}
	});

	// create html
	pandoc(input, ['-f', 'markdown', '-t', 'html', '-o', './' + outputDir + '/dokument.html', '--template=./node_modules/sbp-byggpaket-node/templates/default.html'], function(err) {
		if(err) {
			console.log(err);
		} else {
			console.log('Infopack built without errors!');
		}
	});

	// create pdf
	// pandoc('./' + contentDir + '/index.md', '-f markdown -t latex -o ./' + outputDir +'/dokument.pdf', function(err) {
	// 	if(err) {
	// 		console.log(err);
	// 	} else {
	// 		console.log('Infopack built without errors!');
	// 	}
	// });
};
