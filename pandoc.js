var stat = require('fs').stat;
var spawn = require('child_process').spawn;

module.exports = function(input, args, callback) {

  var result = "";

  // Create child_process.spawn
  var pdSpawn = spawn('pandoc', args);

  pdSpawn.stdin.end(input);

  // Set handlers...
  pdSpawn.stdout.on('data', function (data) {
    result += data;
  });

  pdSpawn.stdout.on('end', function () {
    callback(null, result || true);
  });

  pdSpawn.stderr.on('data', function (err) {
    callback(new Error(err));
  });
};
